[![Build Status](https://app.bitrise.io/app/2d77b076b2b1520d/status.svg?token=2iEZlQWFpanMDWg8bjJFig)](https://app.bitrise.io/app/2d77b076b2b1520d)
# IFSC Checker
## Author: github.com/0x0is1
## IG: instagram.com/0x0is1

Collect bunch of data of account by it's ifsc in your terminal.

### Features:
### using  <a href="https://ifsc.bankifsccode.com">this</a> for data

## Legal disclaimer:

IFSC Checker doesn't have any intention to hurt ssomeones feeling, you can use it know legal bank details. Thanks for Visiting.

### Usage:
```
git clone https://github.com/0x0is1/IFSC-Checker
cd IFSC-Checker
pip install -r req.txt
python3 checkifsc.py
```

### Donate!
Support the authors on<a href="https://paypal.me/0x0is1?locale.x=en_GB">Paypal</a>
